#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "tree.h"
using namespace std;

void main()
{
	Tree<string> mytree;
	ifstream file("test.txt");
	string sentence = "";
	string word = "";
	int line = 1; //sentence number
	vector<string>store; //store sentence	
	
	while (getline(file, sentence)) { //insert data in vector 
		istringstream hold (sentence); //hold data of line
		store.push_back(sentence);
		
		while (getline(hold, word, ' '/*make split word with spacebar*/)) { //insert data in tree
			word = word+" "+char(line + 48); //specify line from word //49 dec = 1 in ascii table
			mytree.insert(word);
		}
		line++;
	}

	cout << "Output each line \n";
	for (int i = 0; i < store.size(); i++) {
		cout << i + 1 << " : " << store[i] << endl;
	}
	cout << "\nOutput from tree: \n";
	mytree.inorder();

	
}